#![allow(unused_imports)]

use std::fs::File;
use std::io::Write;
use std::env;
use std::process;


pub mod waveforms;
use crate::waveforms::sine_table;
use crate::waveforms::noise_table;
use crate::waveforms::triangle_table;

struct WaveHeader
{
    chunk_id : u32,
    chunk_size : u32,
    format : u32,
    subchunk1_id : u32,
    subchunk1_size : u32,
    audio_format : u16,
    num_channels : u16,
    sample_rate : u32,
    byte_rate : u32,
    block_align : u16,
    bits_per_sample : u16,
    subchunk2_id : u32,
    subchunk2_size : u32
}

fn main()
{
    let args : Vec<_> = env::args().collect();

    println!("WaveGen 0.1 by R2AIV");
    println!("---------------------");

    if args.len() != 5
    {
        
        println!("USAGE: ");
        println!("wavegen <wave: sine/noise/triangle> <duration (secs)> <frequency (kHz)> <filename>");
        println!("If generation noise, frequency parameter is ignored.");
        println!("Example: == wavegen sine 5.0 0.44 outfile.wav ==");
        println!("Example: == wavegen noise 5.0 0.44 outfile.wav ==");
        println!("Wrong arguments given!");        
        process::exit(0);
    }
    
    let waveform = args[1].parse::<String>().expect("Wrong mode");
    let duration = args[2].parse::<f32>().expect("Wrong argument format!");
    let freq = args[3].parse::<f32>().expect("Wrong argument format!");
    let filename = args[4].parse::<String>().expect("Wrong filename");

    let mut my_header = WaveHeader
    {
        chunk_id : 0x52494646,          // "RIFF"
        chunk_size : 0x00000000,        // 
        format : 0x57415645,            // "WAVE"
        subchunk1_id : 0x666D7420,      // "fmt "
        subchunk1_size : 16,    
        audio_format : 1,
        num_channels : 1,
        sample_rate : 44100,           // 41100 big endian
        byte_rate : 44100,
        block_align : 1,
        bits_per_sample : 8,
        subchunk2_id : 0x64617461,      // "data"
        subchunk2_size : 0x00000000     // Buffer size
    };
    
    let mut f_desc = File::create(filename.clone()).unwrap();

    // Generating output table
    let mut out_buff = Vec::<u8>::new();
    let mut tmp_table = Vec::<u8>::new();

    // Chosing waveform to generate
    if      waveform == "sine"      {tmp_table = sine_table(duration, freq); }
    else if waveform == "noise"     {tmp_table = noise_table(duration);      }
    else if waveform == "triangle"  {tmp_table = triangle_table(duration, freq);   }
    for j in 0..tmp_table.len()
    {
        out_buff.push(tmp_table[j]);
    }

    println!("Output length: {} bytes",out_buff.len());
    my_header.subchunk2_size = out_buff.len() as u32;
    my_header.chunk_size = my_header.subchunk2_size + 36;

    println!("Writing {}...",filename);

    // Запись заголовка WAV файла. 
    // Строки передаются как big-endian, числа как little-endian (be, le)
    f_desc.write_all(&my_header.chunk_id.to_be_bytes()).unwrap();
    f_desc.write_all(&my_header.chunk_size.to_le_bytes()).unwrap();
    f_desc.write_all(&my_header.format.to_be_bytes()).unwrap();
    f_desc.write_all(&my_header.subchunk1_id.to_be_bytes()).unwrap();
    f_desc.write_all(&my_header.subchunk1_size.to_le_bytes()).unwrap();
    f_desc.write_all(&my_header.audio_format.to_le_bytes()).unwrap();
    f_desc.write_all(&my_header.num_channels.to_le_bytes()).unwrap();
    f_desc.write_all(&my_header.sample_rate.to_le_bytes()).unwrap();
    f_desc.write_all(&my_header.byte_rate.to_le_bytes()).unwrap();
    f_desc.write_all(&my_header.block_align.to_le_bytes()).unwrap();
    f_desc.write_all(&my_header.bits_per_sample.to_le_bytes()).unwrap();
    f_desc.write_all(&my_header.subchunk2_id.to_be_bytes()).unwrap();
    f_desc.write_all(&my_header.subchunk2_size.to_le_bytes()).unwrap();
    
    for _i in 0..out_buff.len() as usize
    {
        f_desc.write_all(&out_buff[_i].to_le_bytes()).unwrap();
        let prog = _i as f32 / out_buff.len() as f32 * 100.0;
        print!("Writing buffer: {}% \r", prog as u8);
    }
    println!("");

    println!("WAV file was written!");
}
