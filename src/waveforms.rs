#![allow (unused_variables)]
#![allow(unreachable_code)]
use rand::Rng;

// Формирование таблицы шума
pub fn noise_table(duration: f32) -> Vec<u8>
{
    let mut rng = rand::thread_rng();
    let mut table = Vec::<u8>::new();

    // Длительность звучания
    let length = (duration + 1.0) as u32 * 41100;
    for _i in 0..length
    {        
        let x = rng.gen::<u8>();
        table.push(x);
    }
    table
}

// Формирование таблицы синуса
pub fn sine_table(duration : f32, freq : f32) -> Vec<u8>
{
    let mut table = Vec::new();
    
    let mut angle = 0.0;
    
    // Шаг 8.16 получен из 360/(44100/360) - 1кГц 
    let angle_step = freq * 8.16;

    // Длительность звучания
    let length = duration * freq * 360000.0;

    while angle <= length
    {
        let sine_val = |angle| {((angle as f32 * 3.141592 / 180.0).sin() * 250.0) / 2.0 + 127.0};
        table.push(sine_val(angle) as u8);
        angle += angle_step;

        let prog = angle as f32 / length as f32 * 100.0;
        print!("Generating sine table: {}% \r", prog as u8);                
    }
    println!("");
    table
}

// Формирование таблицы треугольника
pub fn triangle_table(duration : f32, freq : f32) -> Vec<u8>
{
    let mut table = Vec::<u8>::new();
    table.push(0);
    table
}
